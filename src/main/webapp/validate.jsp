<%-- 
    Document   : validate
    Created on : 22-Apr-2016, 16:55:36
    Author     : Selvyn
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.io.*,java.util.*" %>
<%@page import="com.celestial.dbutils.UserHandler" %>
<%@page import="com.celestial.dbutils.User" %>
<jsp:useBean id="globalHelper" class="com.celestial.loginmanager.ApplicationScopeHelper" scope="application"/>
<jsp:useBean id="theClock" class="com.celestial.simplemavenwebapp.TimeAndDateHandler" scope="session"></jsp:useBean>

<%
    String userId = request.getParameter("id");
    String userPwd = request.getParameter( "password" );
    String loginResponse = "<h1>User ID and Password are INVALID</h1>";

    User theUser = globalHelper.userLogin(userId, userPwd);
    if( theUser != null )
    {
%>
<%--
        <h1><%=theClock.getDate()%> - <%=theClock.getTime()%></h1>
--%>
<%        
        //loginResponse = "<h1>User details are VALID</h1>";
        //loginResponse += "<p>" + UserHandler.getLoader().toJSON(theUser) + "</p>";
        loginResponse = UserHandler.getLoader().toJSON(theUser);
        
        // Do not send this next line back, the jQuery script will fail to parse
        // only send it back as a debug message
        //out.println("Message from server");
        // Sending '\n' before the JSON string will ensure the stream has been flushed
        // so simply call flush instead
        //out.write('\n');
        out.flush();
        out.print( loginResponse );
        out.flush();
    }
%>
<%--
    The Scriplet below has the same effect as writing 
    out.write('\n');
    out.print( loginResponse );
    To use it remove the -- and comment out the out.flush() and out.print() statementts above
--%>
<%--=loginResponse--%>